FROM debian:stretch-slim

ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

RUN apt-get update --fix-missing && \
    apt-get install -y apt-transport-https git wget curl nano tmux python-pip \
    python python2.7-dev python-numpy python-matplotlib \
    python-pandas python-biopython python-scipy \
    python-xlsxwriter python-seaborn python-statsmodels python-deap && \
    echo "deb https://mistertea.github.io/debian-et/debian-source/ stretch main" | tee -a /etc/apt/sources.list && \
    curl -sS https://mistertea.github.io/debian-et/et.gpg | apt-key add - && \
    apt-get update && \
    apt-get install -y et && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean && \
    pip install pysam==0.11.2.2 jupyterlab pylint

EXPOSE 22

RUN echo "export PYTHONPATH="/NGStoolkit"" >> /root/.bashrc && \
    echo "cd /NGStoolkit/" >> /root/.bashrc

COPY docker-entrypoint.sh /usr/local/bin/

RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ENTRYPOINT ["docker-entrypoint.sh"]